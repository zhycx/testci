from dataclasses import dataclass
from encodings import utf_8
from fastapi import FastAPI, Response
import aiohttp
import asyncio


app = FastAPI()


@app.get("/")
async def hello():
    return Response(content="hello admin")



@app.get("/course")
async def main(studentid : int):
    async with aiohttp.ClientSession() as session:
        async with session.post("https://be-prod.redrock.cqupt.edu.cn/magipoke-jwzx/kebiao", data={"stu_num":studentid}) as response:
            data = (await response.json())["data"]
            s = set()
            for c in data:
                s.add(c["course"])
        async with session.post("https://be-prod.redrock.cqupt.edu.cn/magipoke-jwzx/examSchedule", data={"stu_num":studentid}) as response:
            data = (await response.json())["data"]
            m = set()
            for c in data:
                m.add(c["course"])
        s_not_in_m = [i for i in s if i not in m]
        return s_not_in_m
