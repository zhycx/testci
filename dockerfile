#基础镜像
FROM python:3.7
 
ENV APP_HOME /work

COPY  . /work

WORKDIR $APP_HOME 

RUN pip install pandas==1.1.3 FastAPI asynciO Response aiohttp

EXPOSE 80